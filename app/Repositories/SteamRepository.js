import {Utils} from "../../modules/utils.js";

let utils = new Utils();

export class SteamRepository {
    static url = (id) => `https://steamcommunity.com/inventory/${id}/730/2?l=en`;

    static async inventory({}, req) {
        let { id } = req.params;
        if (id === null || id === undefined)
            throw {
                message: "Для выполнения запроса укажите STEAM ID"
            };

        let { tradable = -1 } = req.query;

        if (tradable !== -1) {
            tradable = parseInt(tradable);
            if (isNaN(tradable)) throw {
                message: "Фильтр tradable поддерживает значения [-1, 0, 1]"
            }
        }

        let request;

        try {
            request = await utils.request('GET', this.url(id), {}, {});
        } catch (e) {
            throw {
                status: "404",
                message: "Произошла ошибка во время выполнения запроса."
            }
        }

        let { assets, descriptions } = request;

        if (descriptions === undefined || !descriptions)
            throw {
                status: "404",
                message: "Возможно Вы ввели неверный STEAM ID."
            }

        descriptions = descriptions.map(item => {
            return {
                market_hash_name: item.market_hash_name,
                tradable: item.tradable || 0
            }
        });

        descriptions.sort((a, b) => (a.market_hash_name > b.market_hash_name ? 1 : -1));

        if (tradable !== -1) descriptions = descriptions.filter(item => item.tradable === tradable)

        return descriptions;
    }
}