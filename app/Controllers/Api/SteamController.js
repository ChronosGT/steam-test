import {SteamRepository} from "../../Repositories/SteamRepository.js";

export class SteamController {
    static async inventory(req, res, next) {
        try {
            return res.status(200).json(await SteamRepository.inventory(req.body, req));
        } catch (e) {
            return res.status(e.status || 400).send(e.message || { message: "Возникла ошибка свяжитесь с поддержкой." });
        }
    }
}