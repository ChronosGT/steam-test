import axios from "axios";

export class Utils {
    /**
     * Универсальный метод для выполнения запросов
     * @param {string} method
     * @param {string} url
     * @param {Object} data
     * @param {Object} headers
     * @return {Promise<Object>}
     */
    async request(method, url, data, headers) {
        method = method.toUpperCase();

        let result;

        try {
            result = await axios({
                method,
                url,
                data,
                headers
            });
        } catch (e) {
            throw {
                message: `Ошибка в момент выполнения запроса (${method} ${url})`
            }
        }

        return result.data;
    }
}