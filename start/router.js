import express from "express";
import http from "http";
import cors from "cors";
import compression from "compression";
import helmet from "helmet";

import cookie_parser from "cookie-parser";
import body_parser from "body-parser";

import log from "../modules/log.js"
import appConfig from "../config/app.js"

import api from "../router/api.js";

export class Router {
    async start() {
        const app = express();
        const server = http.createServer(app);

        app.enable("trust proxy");
        app.use(helmet.dnsPrefetchControl());
        app.use(helmet.expectCt());
        app.use(helmet.frameguard());
        app.use(helmet.hidePoweredBy());
        app.use(helmet.hsts());
        app.use(helmet.ieNoOpen());
        app.use(helmet.noSniff());
        app.use(helmet.permittedCrossDomainPolicies());
        app.use(helmet.referrerPolicy());
        app.use(helmet.xssFilter());
        app.use(compression());
        app.use(body_parser.json({limit: '50mb'}));
        app.use(body_parser.urlencoded({limit: '50mb', extended: true}));
        app.use(cookie_parser());
        app.use(cors());

        await api(app);

        server.listen(appConfig.port, 'localhost', () => {
            log.info('Приложение запущено! Прослушивает порт - ' + appConfig.port);
        });
    }
}

