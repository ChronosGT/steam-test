import events from "events";

import {Router} from "./start/router.js";

events.EventEmitter.defaultMaxListeners = 0;

(async () => {
    await new Router().start();
})();