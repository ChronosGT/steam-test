export default {
    port: process.env.NODE_ENV === "production" ? '5605' : '5606',
}