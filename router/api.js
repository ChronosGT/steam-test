import express from "express";

import {SteamController} from "../app/Controllers/Api/SteamController.js";

const apiRouter = express.Router();

export default async (app) => {
    /**
     * GET USER INVENTORY
     */
    apiRouter.get('/inventory/:id?', SteamController.inventory);

    app.use('/v2/api', apiRouter);
}